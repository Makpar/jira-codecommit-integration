
from typing import Any, Dict, Union
import os
import json
from datetime import datetime
import requests
from requests.auth import HTTPBasicAuth
from requests.exceptions import RequestException
import boto3
from botocore.exceptions import ClientError

ATLASSIAN_TOKEN = os.getenv('ATLASSIAN_TOKEN')
def get_commit_id_tree(client, commit_id, repository_name, final_commit):

    print('evaluating commit ', commit_id)
    if commit_id == final_commit:
        print('reached bottom of the tree')
        return []

    commit_ids = []
    commit_ids.append(commit_id)

    commit_info = client.get_commit(
        repositoryName=repository_name,
        commitId=commit_id
    )

    parents = commit_info['commit']['parents']

    print('parents ', parents)
    for parent_id in parents:
        print('retrieving parent id commit tree :', parent_id)
        commit_ids = commit_ids + get_commit_id_tree(client, parent_id, repository_name, final_commit)
        if parent_id == final_commit:
            break

    return commit_ids

def lambda_handler(event: Dict[str, Any], context: Any) -> Dict[str, Union[int, str, Dict[str, Any]]]:

    repository_name = event['detail']['repositoryName']
    commit_id = event['detail']['commitId']
    old_commit_id = event['detail']['oldCommitId']
    
    try:
        client = boto3.client('codecommit')
        commit_ids = get_commit_id_tree(client, commit_id, repository_name, old_commit_id)

        for new_commit_id in commit_ids:
            commit_info = client.get_commit(
                repositoryName=repository_name,
                commitId=new_commit_id
            )
            commit_message = commit_info['commit']['message']
            issue_tag = commit_message.split(' ')[0]
            issue_message = ' '.join(commit_message.split(' ')[1:])

            if '-' in issue_tag:
                headers = {
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                }
                payload = json.dumps({"body": {
                                        "type": "doc",
                                        "version": 1,
                                        "content": [{
                                            "type": "paragraph",
                                            "content": [{
                                                    "text": issue_message,
                                                    "type": "text"
                                                }]
                                    }]}
                                })
                creds = HTTPBasicAuth('gmoore@makpar.com', ATLASSIAN_TOKEN)

                r = requests.post(f'https://makpar.atlassian.net/rest/api/3/issue/{issue_tag}/comment', 
                                    data=payload, auth=creds, headers=headers)
                print('response')
                print(r.text)

    except (ClientError, RequestException) as e:
        print(e)
    
    current_time = datetime.now().strftime("%H:%M:%S")
    return { "message": f"Lambda Execution Finished At {current_time}" }


if __name__ == "__main__":
    print(lambda_handler(None, None))
