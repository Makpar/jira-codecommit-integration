import boto3

def get_commit_id_tree(client, commit_id, repository_name, final_commit):

    print('evaluating commit ', commit_id)
    if commit_id == final_commit:
        print('reached bottom of the tree')
        return []

    commit_ids = []
    commit_ids.append(commit_id)

    commit_info = client.get_commit(
        repositoryName=repository_name,
        commitId=commit_id
    )

    parents = commit_info['commit']['parents']

    print('parents ', parents)
    for parent_id in parents:
        print('retrieving parent id commit tree :', parent_id)
        commit_ids = commit_ids + get_commit_id_tree(client, parent_id, repository_name, final_commit)
        if parent_id == final_commit:
            break

    return commit_ids

if __name__=="__main__":
    client = boto3.client('codecommit')
    print(get_commit_id_tree(client, '983c17bb911d575e27b52bdb35907848f073d154', 'innolab-backend', '03cb63dbe0c2b61e38e53afc0ce601a738bae280'))
    pass